#!/usr/bin/perl

use strict;
use warnings;

use Cwd 'abs_path';
use File::Basename qw(basename dirname);
use File::Find;
use URI;
use CGI;
use HTML::TreeBuilder;
use Getopt::Long;
use Mason;

my $dir = dirname(abs_path(__FILE__));
my $outdir = "$dir/output";
my $filemode_cli = 0;
our $indexhtml = "";
our $configpl = "$dir/config.pl";
our $is_mirror = 0;

GetOptions(
    "file" => \$filemode_cli,
    "mirror" => \$is_mirror,
    "config=s" => \$configpl,
    "output|o=s" => \$outdir)
    or die "usage: @[{basename $0}] [options]
options: --config <file>  configure directory locations to pull from
         --file           put 'index.html' in nav links for file:// previewing
         --mirror         adjust output to be the rsyncable mirror module
         -o, --output <dir>  write output to this directory
\n";

# Default configuration which config.pl can override if it wants to.
our $MAIN_URI = URI->new("https://www.chiark.greenend.org.uk/~sgtatham/putty/");
our $BIN_URI = URI->new("https://the.earth.li/~sgtatham/putty/");
our $SNAPSHOT_URI = URI->new("https://tartarus.org/~simon/putty-snapshots/");
our $PREREL_URI = URI->new("https://tartarus.org/~simon/putty-prerel-snapshots/");
our $GIT_URI = URI->new("https://git.tartarus.org/simon/putty.git");
our $GITWEB_URI = URI->new("https://git.tartarus.org/?p=simon/putty.git");
our $MAILLIST_URI = URI->new("https://lists.tartarus.org/mailman/listinfo/putty-announce");
our $WISHLIST = undef;
our $PUTTYDOC = undef;
our $PUTTYSRC = undef;
our $MASONDATADIR = undef;
our $filemode = undef;

-f $configpl && require $configpl;

$filemode = $filemode_cli unless defined $filemode;
$indexhtml = "index.html" if $filemode;

for my $var (qw(WISHLIST PUTTYDOC PUTTYSRC)) {
    warn "\$$var not defined by $configpl. Website will be incomplete.\n"
        unless eval "defined \$$var";
}

my %masonparams = ('comp_root' => "$dir/components");
$masonparams{'data_dir'} = $MASONDATADIR if defined $MASONDATADIR;
my $mason = Mason->new(%masonparams);

mkdir $outdir, 0777;
mkdir "$outdir/wishlist", 0777;
mkdir "$outdir/releases", 0777;

my $static = "$dir/static/";
find({ wanted => sub
       {
           return unless $static eq substr $_, 0, length $static;
           my $path = substr $_, length $static;
           my $outpath = "$outdir/$path";
           if (-d $_) {
               mkdir $outpath, 0777;
           } else {
               open my $fh, "<", $_ or die "$_: open: $!\n";
               my $content = ""; $content .= $_ while <$fh>;
               close $fh;
               &update($outpath, $content);
           }
       },
       no_chdir => 1}, $static);

# Read the vulnerability lists from the wishlist, in advance of
# running any of the Mason code.
our %vulnlists;
if ($WISHLIST) {
    for my $wish (glob $WISHLIST . "/*.html") {
        my $filename = basename($wish);
        next unless $filename =~ m!^vulnlist-(.*)\.html$!;
        my $version = $1;
        my $vulndata = "";
        open my $vulnfh, "<", $wish or die "$filename: open: $!";
        my $printing = 0;
        while (<$vulnfh>) {
            $printing = 0 if m!<a name="startfooter">!;
            $vulndata .= $_ if $printing;
            $printing = 1 if m!<a name="endheader">!;
        }
        close $vulnfh;
        $vulnlists{$version} = $vulndata;
    }
}

for my $page (qw(home changelog docs licence links maillist mirrors team faq
                 feedback keys wishlist wishlist_historic htaccess
                 latest prerel snapshot wishlistbycve privacy)) {
    my $result = $mason->run("/$page");
    my $outfile = "$outdir/" . $result->data->{'outpath'};
    &update($outfile, $result->output);
}

for my $release_mi (glob "$dir/components/releases/*.mi") {
    $release_mi =~ m!/([^/]*).mi$! or die;
    my $release = $1;
    my $result = $mason->run("/release", release=>$1);
    my $outfile = "$outdir/" . $result->data->{'outpath'};
    &update($outfile, $result->output);
}

if ($WISHLIST) {
    for my $wish (glob $WISHLIST . "/*.html") {
        my $filename = basename($wish);
        next if ($filename eq "index.html" or
                 $filename =~ m!^vulnlist-! or
                 $filename eq "cvelist.html" or
                 $filename eq "historic.html");
        my $result = $mason->run("/wishlistitem", filename=>$filename);
        my $outfile = "$outdir/" . $result->data->{'outpath'};
        &update($outfile, $result->output);
    }
}

sub update {
    my ($outfile, $newdata) = @_;
    my $file;

    # If the file is unchanged, don't rewrite it. Stops needless
    # last-modified-time churn, since my automated website script
    # will be materialising the last mod time in the footer.
    if (-f $outfile && open $file, "<", $outfile) {
        my $prevdata = "";
        $prevdata .= $_ while <$file>;
        close $file;
        return if $prevdata eq $newdata;
    }

    # The file has changed (or wasn't there), so do rewrite it.
    open $file, ">", $outfile or die "$outfile: open: $!\n";
    print $file $newdata;
    close $file;
}
