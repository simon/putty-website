<%class>
has 'release' => (required => 1);
method name() { return "Release " . $.release; }
method httppath() {
    # In a mirror site, the links to the latest version are relative,
    # so that the binary and docs downloads come from the mirror site
    # itself.
    #
    # Also, we should link directly to the release by number, rather
    # than relying on a redirect, so that mirror sites don't need a
    # working .htaccess.
    if ($::is_mirror && $.release eq $.latest_version()) {
        return $.relpath($.outpath(), $.latest_version() . "/");
    }
    # Otherwise, link to the primary site and the exact release number.
    return ${main::BIN_URI} . $.release . "/";
}
method linkver() { return "-" . $.release; }
method textver($type) { return "-" . $.release; }
</%class>
<%method title>Download PuTTY: release <% $.release %></%method>

% my $rel = $m->construct("/releases/" . $.release . ".mi");

<%method header>
<p>
% my $rel = $m->construct("/releases/" . $.release . ".mi");
This page contains
% if ($rel->has_anything()) {
download links for
% } else {
information about
% }
PuTTY release <% $.release %>.

% if ($.release eq $.latest_version()) {
<p>
<% $.release %>, released on <% $rel->date() %>, is the latest release.
You can also find it at the
<a href="<% $.relpath($.outpath(), $.navpath("Stable")) %>">Latest Release page</a>,
which will update when new releases are made (and so is a better page
to bookmark or link to).
% } else {
<p>
<% $.release %>, released on <% $rel->date() %>, is <em>not</em> the latest release.
See the
<a href="<% $.relpath($.outpath(), $.navpath("Stable")) %>">Latest Release page</a>
for the most up-to-date release (currently <% $.latest_version() %>).
% }
</%method>

<% $.header %>

% if ($.release eq $.latest_version()) {
<p>
Release versions of PuTTY are versions we think are reasonably likely
to work well. However, they are often not the most up-to-date
version of the code available. If you have a problem with this
release, then it might be worth trying out
% if (defined $.prerel_version()) {
the <a href="<% $.relpath($.outpath(), $.navpath("Pre-release")) %>">pre-release builds of <% $.prerel_version() %></a>, or
% }
the <a href="<% $.relpath($.outpath(), $.navpath("Snapshot")) %>">development snapshots</a>,
to see if the problem has already been fixed in those versions.
% } else {
<p>
Past releases of PuTTY are versions we thought were reasonably likely
to work well, at the time they were released. However, later releases
will almost always have fixed bugs and/or added new features. If you
have a problem with this release, please try
the <a href="<% $.relpath($.outpath(), $.navpath("Stable")) %>">latest release</a>,
to see if the problem has already been fixed.
% }

% my $category = $.colourcategory($.release);

% if ($category eq "insecure") {
<h2 class="securityboxtop">SECURITY WARNING</h2>
<div class="securityboxbottom">
<p class="securityboxbottomfirst">
This release has known security vulnerabilities.
% if (!defined $::vulnlists{$.latest_version()} || $::vulnlists{$.latest_version()} eq "") {
Consider using a later release instead, such as the <a href="<% $.relpath($.outpath(), $.navpath("Stable")) %>">latest version, <% $.latest_version() %></a>.
% } elsif (defined $.prerel_version() && (!defined $::vulnlists{$.prerel_version()} || $::vulnlists{$.prerel_version()} eq "")) {
Consider using the <a href="<% $.relpath($.outpath(), $.navpath("Pre-release")) %>"><% $.prerel_version() %> pre-release builds</a> instead.
% } elsif (!defined $::vulnlists{"snapshots"} || $::vulnlists{"snapshots"} eq "") {
Consider using the <a href="<% $.relpath($.outpath(), $.navpath("Snapshot")) %>">development snapshots</a> instead.
% } else {
% # I hope very much that this branch is never taken, but just in case...
Unfortunately we have not yet fixed them in any version currently
published.
% }
</p>
<p>
The known vulnerabilities in this release are:
<ul class="securityboxbottomlast"><% $::vulnlists{$.release} %></ul>
</div>
% }

<& version.mi, page=>$self, category=>$category, release=>$rel, callername=>$.name() &>
