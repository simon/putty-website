<%class>method name() { return "Wishlist"; }</%class>
<%method title>PuTTY Known Bugs and Wish List</%method>

% if (defined $::WISHLIST) {
<p>
This is the list of bugs that need fixing, and features that want
adding, to PuTTY. The <a href="#pending">&lsquo;pending&rsquo;</a>
list represents things that have not been done to the current
<em>development</em> code; items marked as &lsquo;fixed&rsquo; may not
yet be in the latest release.

<p>
If you do not see a feature here, it's also worth checking the
<a href="<% $.relpath($.outpath(), $.navpath("Changes")) %>">Changes page</a>
before mailing us to ask for the feature; it might already have
been added.

<p>
There are other sites tracking bugs and feature requests in
downstream packages of the Unix port, some of which undoubtedly
apply to our code:
<a href="https://packages.qa.debian.org/p/putty.html">Debian</a>,
Launchpad
(<a href="https://bugs.launchpad.net/putty">us</a>,
<a href="https://bugs.launchpad.net/ubuntu/+source/putty">Ubuntu</a>),
<a href="ftp://ftp.netbsd.org/pub/pkgsrc/current/pkgsrc/security/putty/README.html">NetBSD</a>.

<p>
Search the wishlist:
<form method="get" action="https://www.google.com/search">
<span class="inlineblock">
<a href="https://www.google.com/"><img src="https://www.google.com/logos/Logo_40wht.gif" border="0" alt="Google"></a>
</span>
<span class="inlineblock">
<input type="hidden" name="hq" value="&#043;&#034;last revision of this bug record&#034;">
<input type="text" name="q" maxlength="255" value="" class="searchedit">
<input type="submit" name="btnG" value="Google Search">
<input type="hidden" name="domains" value="<% $::MAIN_URI->host %>"><br>
<input type="radio" name="sitesearch" value="">WWW
<input type="radio" name="sitesearch" value="<% $::MAIN_URI->host %>" checked><% $::MAIN_URI->host %><br>
</span>
</form>

<p>Here's an index of <a href="cvelist.html">CVE numbers mentioned in
our wishlist entries.</p>

<%perl>

my $srcfile = "$::WISHLIST/index.html";
open my $fh, $srcfile or die "$srcfile: open: $!\n";
while (<$fh>) {
  last if m!<a name="endheader">!i;
}

while (<$fh>) {
  last if m!^</body>!;
  # for each line, we must rewrite hrefs
  s!<a href="\Q${main::MAIN_URI}\E([^"]*)">!'<a href="' . $.relpath($.outpath(), $1) . '">'!eg;
  print;
}

close $fh;

</%perl>
% } else {

<div class="missing">
To generate the parts of this website derived from the separate PuTTY
wishlist repository, define <code>$WISHLIST</code> in
<code>config.pl</code>.
</div>

% }
