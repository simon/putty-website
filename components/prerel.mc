<%class>
method name() { return "Pre-release"; }
method httppath() { return $::PREREL_URI; }
method linkver() { return ""; }
method textver($type) {
    return "-" . $.prerel_version()
               . ($type eq "installer" ? "-" : "~")
               . "pre</code><i>&lt;version&gt</i><code>";
}
</%class>
<%method title>
% if (defined $.prerel_version()) {
Download PuTTY: pre-releases of <% $.prerel_version() %>
% } else {
PuTTY pre-releases (unavailable)
% }
</%method>

% if (defined $.prerel_version()) {
<p>
This page contains download links for a pre-release version of PuTTY
<% $.prerel_version() %>.

<p>
Pre-release versions of PuTTY are likely to be substantially the same
as the next release, when we release it: we expect to only fix bugs,
and not add any new features that will impact the stability of the
code.

<p>
The <a href="<% $.relpath($.outpath(), $.navpath("Snapshot")) %>">development snapshots</a>
may contain features that are not in this pre-release and will not be
in the <% $.prerel_version() %> release either.

<p>
If you find a bug in this pre-release, then please report it, in the
hope that we can fix it before the actual <% $.prerel_version() %>
release!

% my $category = $.colourcategory($.prerel_version());

% if ($category eq "insecure") {
<h2 class="securityboxtop">SECURITY WARNING</h2>
<div class="securityboxbottom">
<p class="securityboxbottomfirst">
The pre-release branch has known security vulnerabilities.
% if (!defined $::vulnlists{$.latest_version()} || $::vulnlists{$.latest_version()} eq "") {
Consider using the <a href="<% $.relpath($.outpath(), $.navpath("Stable")) %>">latest release <% $.latest_version() %></a> instead.
% } elsif (!defined $::vulnlists{"snapshots"} || $::vulnlists{"snapshots"} eq "") {
Consider using the <a href="<% $.relpath($.outpath(), $.navpath("Snapshot")) %>">development snapshots</a> instead.
% } else {
% # I hope very much that this branch is never taken, but just in case...
Unfortunately we have not yet fixed them in any version currently
published.
% }
</p>
<p>
The known vulnerabilities in this release are:
<ul class="securityboxbottomlast"><% $::vulnlists{$.prerel_version()} %></ul>
</div>
% }

<& version.mi, page=>$self, category=>$category, callername=>$.name() &>

% } else {
<p>
There are no PuTTY pre-release builds currently available. When there
are, this page will be populated with links to them.
% }
