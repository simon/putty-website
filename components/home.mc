<%class>method name() { return "Home"; }</%class>
<%method title>PuTTY: a free SSH and Telnet client</%method>

<p>
PuTTY is a free implementation of SSH and Telnet for Windows and Unix
platforms, along with an <code>xterm</code> terminal emulator. It is
written and maintained primarily by
<a href="https://www.chiark.greenend.org.uk/~sgtatham/">Simon Tatham</a>.

<p>
The latest version is <% $.latest_version() %>.
<a href="latest.html"><b>Download it here</b></a>.

<& cryptolaw.mi &>

<h2>Latest news</h2>

% foreach my $newsitem (@news) {
<& newsitem.mi, newsitem=>$newsitem &>
% }

<h2>Site map</h2>

<ul>
<li>
<a href="licence.html">Licence conditions</a> under which you may
use PuTTY.
<li>
<a href="faq.html">The FAQ</a>.
<li>
<a href="docs.html">The documentation</a>.
<li>
Download PuTTY:
<ul>
<li><a href="latest.html">latest release</a> <% $.latest_version() %>
% if (defined $.prerel_version()) {
<li><a href="prerel.html">pre-releases</a> of <% $.prerel_version() %>
% }
<li><a href="snapshot.html">development snapshots</a>
</ul>
<br>
<li>
Subscribe to the <a href="maillist.html">PuTTY-announce</a>
mailing list to be notified of new releases.
<li>
<a href="feedback.html">Feedback</a> and bug reporting: contact
address and guidelines. <em>Please</em> read the guidelines before
sending us mail; we get a very large amount of mail and it will help
us answer you more quickly.
<li>
<a href="changes.html">Changes in recent releases</a>.
<li>
<a href="wishlist/">Wish list</a> and list of known bugs.
<li>
<a href="links.html">Links</a> to related software and
specifications elsewhere.
<li>
A page about the PuTTY <a href="team.html">team members</a>.
</ul>

<%init>
my @news = grep { $_->can('newstitle') }
           sort { $b->date() cmp $a->date() }
           grep { $_->date() ge "2023-05-01" }
            map { $m->construct($_) }
             ($m->interp()->glob_paths("/releases/*.mi"),
              $m->interp()->glob_paths("/news/*.mi"));
</%init>
