<%class>method date() { return "2021-04-18"; }</%class>
<%method newstitle>Pre-releases of 0.75 now available</%method>
<%method newsbody>
<p>
We're working towards a 0.75 release. Pre-release builds are <a
href="prerel.html">available</a>, and we'd appreciate people testing
them and reporting any issues.
<p>
0.75 will be a feature release. The biggest changes all relate to
Pageant and/or SSH public keys. User-visible behaviour changes include:
<ul>
<li>Pageant now allows you to load a key without decrypting it, in
which case it will wait until you first use it to ask for the
passphrase.</li>
<li>We've switched to the modern OpenSSH-style SHA-256 style of key
fingerprint.</li>
</ul>
Back-end changes that affect compatibility:
<ul>
<li>We've added support for the <code>rsa-sha2-256</code> and
<code>rsa-sha2-512</code> signature methods, which some servers now
require in order to use RSA keys.</li>
<li>We've introduced a new version of the PPK format for private key
files, to remove weak crypto and improve password-guessing
resistance.</li>
<li>We've introduced a new method for applications to talk to Pageant
on Windows, based on the same named-pipe system used by connection
sharing instead of window messages.</li>
</ul>
</%method>
