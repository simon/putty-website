<%class>method name() { return "Licence"; }</%class>
<%method title>PuTTY Licence</%method>

<p>The PuTTY executables and source code are distributed under the
MIT licence, which is similar in effect to the BSD licence. (This
licence is
<a href="https://opensource.org/licenses/">Open Source certified</a>
and complies with the
<a href="https://www.debian.org/social_contract">Debian Free Software Guidelines</a>.)

<p>The precise licence text, as given in the About box and in the
file LICENCE in the source distribution, is as follows:

% if (defined $::PUTTYSRC) {

<blockquote>

<%perl>

my $licpath = "${main::PUTTYSRC}/LICENCE";

my @para = "";
my $dumppara = sub {
    printf "<p><i>%s</i></p>\n\n", join "\n", @para if @para;
    @para = ();
};

open my $fh, "<". $licpath or die "$licpath: open: $!\n";
while (<$fh>) {
    chomp;
    if ($_ eq "") {
        $dumppara->();
    } else {
        push @para, CGI::escapeHTML($_);
    }
}
$dumppara->();
close $fh;

</%perl>

</blockquote>

% } else {

<div class="missing">
To generate the licence text from the <code>LICENCE</code> file in the
main PuTTY repository, define <code>$PUTTYSRC</code> in
<code>config.pl</code>.
</div>

% }

<p>In particular, anybody (even companies) can use PuTTY without
restriction (even for commercial purposes) and owe nothing to us, or
to anybody else. Also, apart from having to maintain the copyright
notice and the licence text in derivative products, anybody (even
companies) can adapt the PuTTY source code into their own programs
and products (even commercial products) and owe nothing to us or
anybody else. And, of course, there is no warranty and if PuTTY
causes you damage you're on your own, so don't use it if you're
unhappy with that.
</p>

<p>
In particular, note that the MIT licence is compatible with the GNU
GPL. So if you want to incorporate PuTTY or pieces of PuTTY into a
GPL program, there's no problem with that.
</p>
