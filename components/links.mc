<%class>method name() { return "Links"; }</%class>
<%method rfclink ($id)>https://www.rfc-editor.org/rfc/rfc<% $id %></%method>
<%method title>PuTTY Links</%method>

<h2>Related software</h2>

<p>Several pieces of third-party software incorporate parts of the
PuTTY code, or augment PuTTY in other ways, to provide facilities not
available from PuTTY itself.  We list some of them here, with no
recommendation implied.  We have no control over this code, so we
can't vouch for either its quality or its security.

<h3>Software based on PuTTY's code</h3>

<p>These projects include actual code from some version of PuTTY.

<ul>

<li>Clients for various operating systems
<ul>
<li><a href="http://nettlessh.mine.nu/">
  NettleSSH</a>, an SSH client for RISC OS
<li><a href="https://www.chiark.greenend.org.uk/~theom/riscos/crypto/#ssh">
  SSHProxy and pscp for RISC OS</a>
<li><a href="http://s2putty.sourceforge.net/">
  PuTTY for Symbian</a> (Nokia)
<li><a href="http://matrix.tmit.bme.hu/putty/">
  PuTTY for Symbian UIQ</a> (discontinued)
<li>Another <a href="http://www.mobileyes.com/index.php?option=com_content&task=view&id=23&Itemid=51">
  PuTTY for UIQ</a>
<li><a href="http://coredump.fi/putty">
  PuTTY for Symbian UIQ3</a>
<li><a href="http://www.pocketputty.net/">
  PocketPuTTY</a>, a port for Windows Mobile 2003
<li><a href="http://mattwesemann.azurewebsites.net/">
  Metro PuTTY</a>, a port to Windows RT (8/10/etc)
  (<a href="https://www.microsoft.com/en-us/store/apps/metro-putty/9wzdncrdndfh">Microsoft Store link</a>)
<li><a href="http://www.sealiesoftware.com/pssh/">
  pssh</a>, a Palm OS SSH-2 client that uses some PuTTY terminal code
<li><a href="http://www.instantcocoa.com/products/pTerm/">
  pTerm</a>, for iPhone and iPod Touch (not to be confused with our
  own Unix terminal emulator "pterm"). Commercial, but cheap.
<li><a href="http://sshdos.sourceforge.net/">
  SSHDOS</a>, SSH, SCP, SFTP and Telnet client for MS-DOS
</ul>

<li><a href="wishlist/gui-sftp.html">Frontends</a> for file transfer
<ul>
<li><a href="http://winscp.net/">
  WinSCP</a>, a GUI SFTP and SCP client
<li><a href="http://filezilla.sourceforge.net/">
  FileZilla</a>, a GUI file transfer client with SFTP support from PuTTY
<li><a href="http://www.enginsite.com/ssh-webdav-ftp-sftp-client.htm">
  DataFreeway</a>, uses PuTTY to provide a Microsoft Explorer interface
  to SSH/SFTP (no source)
</ul>

<li>Port forwarding and VPN</li>
<ul>
<li><a href="http://sourceforge.net/projects/wintunnel">
  Wintunnel</a>, a front end for creating tunnels
</ul>

<li><a href="wishlist/i18n.html">Internationalised and localised</a> versions
<ul>
<li><a href="http://hp.vector.co.jp/authors/VA024651/PuTTYkj.html">
  PuTTYjp</a>, with ISO-2022 support and other things Japanese
<li><a href="http://www.mhsin.org/putty/">
  PuTTY DBCS Patched</a>, with a Chinese feel
<li><a href="http://www.arabeyes.org/project.php?proj=PuTTY">
  Arabeyes PuTTY</a>, a project to add Arabic support to PuTTY
  (this code is now <a href="wishlist/bidi.html">included</a>
  in our releases)
<li><a href="http://kldp.net/projects/iputty/">
  iPuTTY/HangulPuTTY</a>, an internationalised fork with an emphasis on
  Korean support
<li><a href="http://putty.grzybicki.pl/">
  Polish localised version of PuTTY</a> (no source)
</ul>

<li><a href="wishlist/kerberos-gssapi.html">Kerberos/GSSAPI support</a>
(note that 0.61 and later releases contain native Kerberos support for
Windows and Unix)
<ul>
<li><a href="http://www.css-security.com/cgi-bin/dnld_list.pl">
  Certified Security Solutions, Inc.</a>
<li><a href="http://rc.quest.com/topics/putty/">
  Quest PuTTY</a> (formerly Vintela PuTTY)
<li><a href="http://sweb.cz/v_t_m/#putty">
  sweb.cz</a> patch for Windows and Unix
<li><a href="http://www.centrify.com/resources/putty.asp">Centrify PuTTY</a>
  provides GSSAPI support via the Windows SSPI (no source)
</ul>

<li><a href="wishlist/smartcard-auth.html">Smartcard support</a>
<ul>
<li><a href="http://smartcard-auth.de/ssh-en.html">A smartcard-enabled
  Pageant</a>
<li>The <a href="http://www.opensc-project.org/scb/">Smart Card Bundle</a>
  is a packaged version of some of the patches found at
  <a href="http://www.opensc-project.org/files/contrib/">opensc-project.org</a>
<li>PuTTYcard can <em>also</em> be found at
  <a href="http://www.opensc-project.org/files/contrib/">opensc-project.org</a>,
  but doesn't use OpenSC
<li><a href="http://www.joebar.ch/puttysc/">
  PuTTY SC</a>
</ul>

<li><a href="wishlist/config-locations.html">Settings storage in files</a>
<ul>
<li><a href="http://socialistsushi.com/portaputty">portaPuTTY</a>, a version
  which (only) uses files for data storage
<li><a href="http://code.google.com/p/portaputty/">Another portaPuTTY</a>
  (same name, different code)
<li><a href="http://jakub.kotrla.net/putty/">Another version</a> that
  stores data in files
</ul>

<li><a href="wishlist/url-launching.html">URL recognition</a>
<ul>
<li><a href="http://www.groehn.net/nutty/">
  Nutty</a> (not actively developed)
</ul>

<li>Minimise to the <a href="wishlist/system-tray.html">system tray</a>
<ul>
<li><a href="https://puttytray.goeswhere.com/">
  PuTTY Tray</a>, minimising to system tray, transparency,
  <a href="wishlist/url-launching.html">URL hyperlinks</a>,
  <a href="wishlist/config-locations.html">settings storage in files</a>,
  reconnecting on failure, Android adb support
</ul>

<li><a href="wishlist/transparency.html">Transparency</a>
<ul>
<li><a href="http://cprogramming.hu/transputty/">
  transputty</a>
  (<a href="http://www.cs.helsinki.fi/u/mxkoskin/">updated</a>)
<li><a href="http://www.covidimus.net/projects/putty/putty.php">
  Patch at covidimus.net</a>
<li><a href="http://x86code.com/putty-transparent.zip">
  Patch at x86code.com</a>
  (<a href="http://x86code.com/putty-transparent.diff">diff file</a>).
</ul>

<li><a href="wishlist/mud-client.html">MUD clients</a>
<ul>
<li><a href="http://elvenrunes.mine.nu/powtty/">
  PowTTY</a>
<li><a href="http://www.dforces.net/client/">
  DF Client</a>
</ul>

<li>Various combinations of the above
<ul>
<li><a href="http://leputty.sourceforge.net/">
  Le Putty</a>, a fork with <a href="wishlist/zmodem.html">Z-modem</a> support 
  and other tweaks
<li><a href="http://putty.dwalin.ru/">
  TuTTY</a>, a fork with several extra features, including a
  <a href="wishlist/serial-backend.html">serial backend</a>
  (this dates from before PuTTY had its own)
<li><a href="http://ntu.csie.org/~piaip/pietty/">
  PieTTY</a>, a fork with CJK/transparency/URL recognition etc (no source?)
<li><a href="http://kitty.9bis.com/">
  KiTTY</a>, a (Windows-only) fork with several features including
  storing your password, minimising to the
  <a href="wishlist/system-tray.html">system tray</a>,
  <a href="wishlist/config-locations.html">settings storage in files</a>,
  and automatically sending a command (as if typed at the keyboard)
  after successful session startup. (Also <a href="https://github.com/cyd01/KiTTY/">on Github</a>)
<ul>
  <li>Don't confuse this with <a rel="nofollow" href="https://github.com/kovidgoyal/kitty/">the other <code>kitty</code></a>, spelled in lower case. That is also a terminal emulator, but <em>not</em> anything to do with PuTTY!</li>
</ul>
<li><a href="http://www.extraputty.com/">ExtraPuTTY</a>, a (Windows-only) fork
  featuring a DLL wrapper, scripting with lua, URL hyperlinks,
  <a href="wishlist/config-locations.html">settings storage in files</a>,
  bundled <a href="http://puttysm.sourceforge.net/">PuTTY Session Manager</a>,
  and a couple of other things (no source)
</ul>

<li><a href="http://code.google.com/p/puttycyg/">
  PuTTYcyg</a>, to use PuTTY as a terminal for
  <a href="http://www.cygwin.com">Cygwin</a>. (The standard PuTTY can
  just about do this too, with a bit of fiddly configuration including
  compiling a helper program.
  See the
  <a href="wishlist/cygwin-terminal-window.html">cygwin-terminal-window</a>
  wishlist entry for details.)
<li><a href="http://www.tortoisecvs.org/">
  TortoiseCVS</a> and <a href="http://tortoisesvn.tigris.org">
  TortoiseSVN</a>, Windows Explorer frontends to
  <a href="http://savannah.nongnu.org/projects/cvs">CVS</a> and
  <a href="http://subversion.tigris.org/">Subversion</a> respectively,
  use a modified Plink for SSH transport.
<li><a href="http://home.planet.nl/~ruurdb/IVT.HTM">
  IVT</a>, a VT220 emulator which uses some PuTTY code (no source)
<li><a href="http://www.starnet.com/products/StarNetSSH/">
  StarNetSSH</a>, an integration with the X-Win32 X server
<li><a href="http://www.winputty.com/">
  W-PuTTY-CD</a>, a <a href="wishlist/dll-frontend.html">DLL frontend</a>

</ul>

<h3>Other related software</h3>

<ul>

<li><a href="wishlist/gui-sftp.html">Frontends</a> for file transfer
<ul>
<li>Secure iXplorer, a GUI SFTP client; there are
in <a href="http://www.i-tree.org/gpl/ixplorer.htm">
  GPL</a> and <a href="http://www.i-tree.org/secixpro/index.htm">
  commercial ("Pro")</a> versions
</ul>

<li>Port forwarding and VPN</li>
<ul>
<li><a href="http://callinghome.sourceforge.net/">CallingHome</a>, for
  maintaining long-running SSH tunnels using PuTTY.
<li><a href="http://nemesis2.qx.net/software-myentunnel.php">
  MyEntunnel</a>, for maintaining SSH tunnels using Plink (no source)
<li><a href="http://code.google.com/p/putty-tunnel-manager/">PuTTY Tunnel
Manager</a>, for maintaining multiple, long-running sessions each providing a
set of tunnels
</ul>

<li>Saved session management
<ul>
<li><a href="http://www.deckmyn.org/olivier/software">
  QuickPutty</a>, for quickly launching PuTTY saved sessions
<li><a href="http://putty.dwalin.ru/?plaunch">
  plaunch</a>, another program for quickly launching PuTTY saved sessions
<li><a href="http://puttysm.sourceforge.net/">
  PuTTY Session Manager</a>, for organising and launching saved sessions
<li><a href="http://www.thiago.joi.com.br/andre/puttyconfer.html">
  PuttyConfer</a>, for importing, exporting, and mass modification of
  PuTTY sessions
<li><a href="http://www.ntsend.freeserve.co.uk/puttymenu.html">
  PuTTY Menu</a>, for organising, launching, exporting, and importing
  saved sessions
<li><a href="http://www.launchy.net/">Launchy</a> with
  <a href="http://code.google.com/p/putty-launchy-plugin/">PuTTY Plugin</a>
  allows launching PuTTY sessions by typing first characters of their name,
  also works with versions storing sessions in files
</ul>

<li><a href="wishlist/multiple-connections.html">Multiple connection</a>
management (including tabbing)
<ul>
<li><a href="http://www.raisin.de/putty-tabs/putty-tabs.html">
  PuttyTabs</a>, a floating tab bar for PuTTY sessions
<li><a href="http://spoox.org/projects/twsc/">
  TWSC</a> (Terminal Window ShortCuts), provides a menu of open terminal
  windows (no source)
<li><a href="http://www.wintabber.com/">
  WinTabber</a> allows PuTTY sessions (and other programs) to be captured
  into a single tabbed window on Windows 2000/XP (no source)
<li><a href="http://puttymanager.sourceforge.net/">PuTTY Manager</a> allows
  launching PuTTY sessions in a tabbed interface and/or dockable windows
<li><a href="http://code.google.com/p/superputty/">SuperPutty</a> provides
  tabbed/docking interface for PuTTY sessions and file uploads
<li><a href="http://ttyplus.com/multi-tabbed-putty/">MTPuTTY</a> embeds PuTTY
  sessions in a tabbed interface, provides automation features (no source)
</ul>

<li><a href="http://www.kenworld.se/robert/index.asp?dir=Putty%20Magic">PuTTY
  Magic</a> provides borderlessness, transparency, and alphablending
  in PuTTY
<li><a href="http://www.millardsoftware.com/puttycs">
  PuTTY Command Sender</a>, for sending commands to and otherwise
  manipulating <a href="wishlist/terminal-fanout.html">multiple PuTTY
  windows at once</a>
<li><a href="https://github.com/munawarb/JAWS-PuTTY-Scripts">
  JAWS PuTTY Scripts</a>, to make the JAWS screen reader work better
  with PuTTY</a>

</ul>

<h2>Specifications implemented by PuTTY</h2>

<p>
PuTTY attempts to conform to many specifications.  These include:

<h3>SSH-2 specifications</h3>
<ul>
<li><a href="<% $.rfclink(4250) %>">
  RFC 4250: The Secure Shell (SSH) Protocol Assigned Numbers</a>
<li><a href="<% $.rfclink(4251) %>">
  RFC 4251: The Secure Shell (SSH) Protocol Architecture</a>
<li><a href="<% $.rfclink(4252) %>">
  RFC 4252: The Secure Shell (SSH) Authentication Protocol</a>
<li><a href="<% $.rfclink(4253) %>">
  RFC 4253: The Secure Shell (SSH) Transport Layer Protocol</a>
<li><a href="<% $.rfclink(4254) %>">
  RFC 4254: The Secure Shell (SSH) Connection Protocol</a>
<li><a href="<% $.rfclink(4256) %>">
  RFC 4256: Generic Message Exchange Authentication for the Secure Shell
  Protocol (SSH)</a>
  <ul>
  <li>(more usually known as <code>keyboard-interactive</code>)</li>
  </ul>
<li><a href="<% $.rfclink(4335) %>">
  RFC 4335: The Secure Shell (SSH) Session Channel Break Extension</a>
<li><a href="<% $.rfclink(4344) %>">
  RFC 4344: The Secure Shell (SSH) Transport Layer Encryption Modes</a>
  (in part)
<li><a href="<% $.rfclink(4345) %>">
  RFC 4345: Improved Arcfour Modes for the Secure Shell (SSH) Transport Layer Protocol</a>
<li><a href="<% $.rfclink(4419) %>">
  RFC 4419: Diffie-Hellman Group Exchange for the Secure Shell (SSH) Transport Layer Protocol</a>
<li><a href="<% $.rfclink(4432) %>">
  RFC 4432: RSA Key Exchange for the Secure Shell (SSH) Transport Layer Protocol</a>
<li><a href="<% $.rfclink(4462) %>">
  RFC 4462: Generic Security Service Application Program Interface (GSS-API) Authentication and Key Exchange for the Secure Shell (SSH) Protocol</a>
<li><a href="<% $.rfclink(4716) %>">
  RFC 4716: The Secure Shell (SSH) Public Key File Format</a>
<li><a href="<% $.rfclink(5647) %>">
  RFC 5647: AES Galois Counter Mode for the Secure Shell Transport Layer Protocol</a>
  <ul>
  <li>(or rather, the <tt>@openssh.com</tt> protocol variants defined in <a href="https://cvsweb.openbsd.org/src/usr.bin/ssh/PROTOCOL?rev=HEAD&content-type=text/x-cvsweb-markup">OpenSSH's protocol extension documentation</a>)
  </ul>
<li><a href="<% $.rfclink(5656) %>">
  RFC 5656: Elliptic Curve Algorithm Integration in the Secure Shell Transport Layer</a>
<li><a href="<% $.rfclink(6668) %>">
  RFC 6668: SHA-2 Data Integrity Verification for the Secure Shell (SSH) Transport Layer Protocol</a>
  <ul>
  <li>(<tt>hmac-sha2-256</tt> only; we don't implement <tt>hmac-sha2-512</tt>)</li>
  </ul>
<li><a href="<% $.rfclink(8160) %>">
  RFC 8160: IUTF8 Terminal Mode in Secure Shell (SSH)</a>
<li><a href="<% $.rfclink(8268) %>">
  RFC 8268: More Modular Exponentiation (MODP) Diffie-Hellman (DH) Key Exchange (KEX) Groups for Secure Shell (SSH)</a>
<li><a href="<% $.rfclink(8308) %>">
  RFC 8308: Extension Negotiation in the Secure Shell (SSH) Protocol</a>
  <ul>
  <li>(<tt>server-sig-algs</tt>, to enable RFC 8332)
  </ul>
<li><a href="<% $.rfclink(8332) %>">
  RFC 8332: Use of RSA Keys with SHA-256 and SHA-512 in the Secure Shell (SSH) Protocol</a>
<li><a href="<% $.rfclink(8709) %>">
  RFC 8709: Ed25519 and Ed448 Public Key Algorithms for the Secure Shell (SSH) Protocol</a>
<li><a href="<% $.rfclink(8731) %>">RFC 8731: Secure Shell (SSH) Key Exchange Method Using Curve25519 and Curve448</a>
<li><a href="<% $.rfclink(8732) %>">RFC 8732: Generic Security Service Application Program Interface (GSS-API) Key Exchange with SHA-2</a>
<li><a href="https://datatracker.ietf.org/group/secsh/about/">
  IETF Secure Shell working group</a> drafts:
  <ul>
  <li><a href="https://datatracker.ietf.org/doc/html/draft-ietf-secsh-filexfer">filexfer</a>
  </ul>
<li>Other drafts:
  <ul>
  <li><a href="https://datatracker.ietf.org/doc/html/draft-miller-secsh-compression-delayed">draft-miller-secsh-compression-delayed</a>
  <li><a href="https://datatracker.ietf.org/doc/html/draft-miller-ssh-agent">draft-miller-ssh-agent</a>
  </ul>
<li>Documents not published as Internet-Drafts or RFCs:
  <ul>
  <li><a href="https://cvsweb.openbsd.org/src/usr.bin/ssh/PROTOCOL.chacha20poly1305?rev=HEAD&content-type=text/x-cvsweb-markup">OpenSSH's spec for the ChaCha20-Poly1305 cipher and MAC</a>
  <li><a href="https://cvsweb.openbsd.org/src/usr.bin/ssh/PROTOCOL.key?rev=HEAD&content-type=text/x-cvsweb-markup">OpenSSH's spec for their private key format</a>
  </ul>
</ul>

<h3>SSH-1 specification</h3>
<ul>
<li><a href="http://www.snailbook.com/docs/protocol-1.5.txt">
  The SSH (Secure Shell) Remote Login Protocol</a>
<li>We don't know of a reference for the SCP protocol (which is
  basically BSD rcp).
</ul>

<h3>Telnet specifications</h3>
<ul>
<li><a href="<% $.rfclink(854) %>">
  RFC 854: TELNET Protocol Specification</a>
<li><a href="<% $.rfclink(855) %>">
  RFC 855: TELNET Option Specifications</a>
<li><a href="<% $.rfclink(856) %>">
  RFC 856: TELNET Binary Transmission</a>
<li><a href="<% $.rfclink(857) %>">
  RFC 857: TELNET Echo Option</a>
<li><a href="<% $.rfclink(858) %>">
  RFC 858: TELNET Suppress Go Ahead Option</a>
<li><a href="<% $.rfclink(1073) %>">
  RFC 1073: Telnet Window Size Option</a>
<li><a href="<% $.rfclink(1079) %>">
  RFC 1079: Telnet Terminal Speed Option</a>
<li><a href="<% $.rfclink(1091) %>">
  RFC 1091: Telnet Terminal-Type Option</a>
<li><a href="<% $.rfclink(1123) %>">
  RFC 1123: Requirements for Internet Hosts -- Application and Support</a>
<li><a href="<% $.rfclink(1408) %>">
  RFC 1408: Telnet Environment Option</a>
<li><a href="<% $.rfclink(1571) %>">
  RFC 1571: Telnet Environment Option Interoperability Issues</a>
<li><a href="<% $.rfclink(1572) %>">
  RFC 1572: Telnet Environment Option</a>
</ul>

<h3>Rlogin specification</h3>
<ul>
<li><a href="<% $.rfclink(1282) %>">
  RFC 1282: BSD Rlogin</a>
</ul>

<h3>SUPDUP specification</h3>
<ul>
<li><a href="<% $.rfclink(734) %>">
  RFC 734: SUPDUP Protocol</a>
</ul>

<h3>HTTP specifications</h3>
<ul>
<li><a href="<% $.rfclink(2616) %>">
  RFC 2616: Hypertext Transfer Protocol -- HTTP/1.1</a>
<li><a href="<% $.rfclink(2617) %>">
  RFC 2617: HTTP Authentication: Basic and Digest Access Authentication</a>
<li><a href="<% $.rfclink(2817) %>">
  RFC 2817: Upgrading to TLS Within HTTP/1.1</a> (HTTP CONNECT)
<li><a href="<% $.rfclink(7616) %>">
  RFC 7616: HTTP Digest Access Authentication</a> (updates RFC 2617 with extra hash functions and more features)
</ul>

<h3>SOCKS specifications</h3>
<ul>
<li><a href="https://www.openssh.com/txt/socks4.protocol">
  SOCKS: A protocol for TCP proxy across firewalls</a> (SOCKS 4)
<li><a href="https://www.openssh.com/txt/socks4a.protocol">
  SOCKS 4A: A  Simple Extension to SOCKS 4 Protocol</a>
<li><a href="<% $.rfclink(1928) %>">
  RFC 1928: SOCKS Protocol Version 5</a>
<li><a href="<% $.rfclink(1929) %>">
  RFC 1929: Username/Password Authentication for SOCKS V5</a>
<li><a href="https://datatracker.ietf.org/doc/html/draft-ietf-aft-socks-chap-01">
  Challenge-Handshake Authentication Protocol for SOCKS V5</a>
  is only available as an expired Internet-Draft.
</ul>

<h3>Terminal specifications</h3>
<ul>
<li><a href="http://www.ecma-international.org/publications/standards/ECMA-035.htm">
  ECMA-35: Character Code Structure and Extension Techniques</a>
  (equivalent to ISO 2022)
<li><a href="http://www.ecma-international.org/publications/standards/ECMA-048.HTM">
  ECMA-48: Control Functions for Coded Character Sets</a>
  (equivalent to ISO 6429)
</ul>

<h3>Zlib compressed data format</h3>
<ul>
<li><a href="<% $.rfclink(1950) %>">
  RFC 1950: ZLIB Compressed Data Format Specification</a>
  (this is mostly just wrapping and red tape)
<li><a href="<% $.rfclink(1951) %>">
  RFC 1951: DEFLATE Compressed Data Format Specification</a>
  (this contains the details of the actual compressed data)
</ul>

<h3>Miscellaneous</h3>
<ul>
<li><a href="<% $.rfclink(4648) %>">
  RFC 4648: The Base16, Base32, and Base64 Data Encodings</a>
  (PuTTY only uses Base64 out of these)
</ul>

<h3>X Window System</h3>
<ul>
<li><a href="https://www.x.org/releases/current/doc/xproto/x11protocol.html">
  X Window System Protocol</a>
<li><a href="https://www.x.org/releases/current/doc/libXdmcp/xdmcp.html">
  X Display Manager Control Protocol</a>,
  including the definition of XDM-AUTHORIZATION-1.
<li><a href="https://www.x.org/releases/current/doc/man/man7/Xsecurity.7.xhtml">
  Xsecurity(7)</a>, which documents MIT-MAGIC-COOKIE-1.
</ul>

<h3>Cryptographic algorithms</h3>
<ul>
<li><a href="http://csrc.nist.gov/publications/fips/fips46-3/fips46-3.pdf">
  FIPS PUB 46-3: Data Encryption Standard (DES)</a> also defines TDEA (3DES).
<li><a href="http://www.schneier.com/blowfish.html">
  Bruce Schneier's page on the Blowfish block cipher</a>.
<li><a href="http://csrc.nist.gov/publications/fips/fips197/fips-197.pdf">
  FIPS PUB 197: Advanced Encryption Standard (AES)</a>
<li><a href="<% $.rfclink(1321) %>">
  RFC 1321: The MD5 Message-Digest Algorithm</a>
<li><a href="http://csrc.nist.gov/publications/fips/fips180-2/fips180-2.pdf">
  FIPS PUB 180-2: Secure Hash Signature Standard (SHS)</a>
  defines secure hash functions SHA-1 and SHA-512.
<li><a href="<% $.rfclink(2104) %>">
  RFC 2104: HMAC: Keyed Hashing for Message Authentication</a>
  defines a generic wrapper mechanism that
  converts a hash function such as MD5 or SHA-1 into a secure MAC.
<li><a href="http://csrc.nist.gov/publications/fips/fips186-2/fips186-2-change1.pdf">
  FIPS PUB 186-2: Digital Signature Standard (DSS)</a> defines the digital
  signature algorithm DSA.
<li><a href="<% $.rfclink(6979) %>">
  RFC 6979: Deterministic Usage of the Digital Signature Algorithm (DSA) and
  Elliptic Curve Digital Signature Algorithm (ECDSA)</a> specifies
  a procedure to avoid the risk of key recovery attacks when generating
  DSA/ECDSA signatures.
<li><a href="<% $.rfclink(3447) %>">
  RFC 3447: Public-Key Cryptography Standards (PKCS) #1: RSA Cryptography
  Specifications version 2.1</a> defines the basic RSA encryption and
  signature algorithms as well as specific padding schemes to turn them into
  a set of well defined operations on byte strings.
<li>The precise form of Diffie-Hellman key exchange used in SSH-2 is
  defined in the SSH-2 transport layer specification: see the SSH-2
  section above.
<li><a href="http://www.secg.org/sec1-v2.pdf">SEC 1: Elliptic Curve Cryptography</a> defines many of the underlying primitives referred to in RFC 5656.
<li><a href="http://ed25519.cr.yp.to/">The Ed25519 website</a>, with specifications and other resources
<li><a href="http://cr.yp.to/chacha/chacha-20080128.pdf">The ChaCha20 cipher specification</a>, specified as modifications to <a href="http://cr.yp.to/snuffle/spec.pdf">the Salsa20 specification</a>
  <ul>
  <li>(note that PuTTY does not directly implement Salsa20; we only cite the specification here because it is necessary to make sense of the ChaCha20 spec)
  </ul>
<li><a href="http://cr.yp.to/mac/poly1305-20050329.pdf">The Poly1305 MAC specification</a>
</ul>
