<%class>method name() { return "Changes"; }</%class>
<%method title>PuTTY Change Log</%method>

<!--
<p>
These features are planned for the next full release (and should
therefore be available in the development snapshots, unless they're
not updating, which happens sometimes):
-->

<p>
For features planned for the next full release (and already available in
the development snapshots), see the <a href="wishlist/">wishlist page</a>.

% foreach my $release (@releases) {
<& changelogitem.mi, release=>$release, latest=>$releases[0], page=>$self &>
% }

<%init>
my @releases = $.list_releases();
</%init>
