<%class>
method name() { return "Snapshot"; }
method httppath() { return $::SNAPSHOT_URI; }
method linkver() { return ""; }
method textver($type) { return "-</code><i>&lt;version&gt</i><code>"; }
</%class>
<%method title>Download PuTTY: latest development snapshot</%method>

<p>
This page contains download links for the latest development snapshot
of PuTTY.

<p>
The development snapshots are built every day, automatically, from the
current development code &#8211; in <em>whatever</em> state it's
currently in. So you may be able to find new features or bug fixes in
these snapshot builds, well before the same changes make it into the
<a href="<% $.relpath($.outpath(), $.navpath("Stable")) %>">latest release</a>.
On the other hand, these snapshots might also be unstable, if a lot of
new development has happened recently and introduced new bugs.

<p>
In particular, they may contain security holes. If we find one that's
not also in a release, we're likely to just fix it in the next day's
snapshot. If it's a particularly bad one we might make a note on
the <a href="wishlist/">wishlist page</a>.

% my $category = $.colourcategory("snapshots");

% if ($category eq "insecure") {
<h2 class="securityboxtop">SECURITY WARNING</h2>
<div class="securityboxbottom">
<p class="securityboxbottomfirst">
The development snapshots have known security vulnerabilities.
% if (!defined $::vulnlists{$.latest_version()} || $::vulnlists{$.latest_version()} eq "") {
Consider using the <a href="<% $.relpath($.outpath(), $.navpath("Stable")) %>">latest release <% $.latest_version() %></a> instead.
% } elsif (defined $.prerel_version() && (!defined $::vulnlists{$.prerel_version()} || $::vulnlists{$.prerel_version()} eq "")) {
Consider using the <a href="<% $.relpath($.outpath(), $.navpath("Pre-release")) %>"><% $.prerel_version() %> pre-release builds</a> instead.
% } else {
% # I hope very much that this branch is never taken, but just in case...
Unfortunately we have not yet fixed them in any version currently
published.
% }
</p>
<p>
The known vulnerabilities in this release are:
<ul class="securityboxbottomlast"><% $::vulnlists{"snapshots"} %></ul>
</div>
% }

<p>
<& version.mi, page=>$self, category=>$category, callername=>$.name() &>
