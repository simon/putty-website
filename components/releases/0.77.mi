<%flags>
extends => '../relbase.mi'
</%flags>
<%class>method version() { return "0.77"; }</%class>
<%class>method date() { return "2022-05-27"; }</%class>
<%method changelog>
<ul>
<li>Major improvements to network proxy support:
<ul>
<li>Support for interactively prompting the user if the proxy server
requires authentication.
<li>Built-in support for proxying via another SSH server, so that
PuTTY will SSH to the proxy and then automatically forward a port
through it to the destination host. (Similar to running <code>plink
-nc</code> as a subprocess, but more convenient to set up, and allows
you to answer interactive prompts presented by the proxy.)
<li>Support for HTTP Digest authentication, when talking to HTTP
proxies.
</ul>
<li>Introduced <code>pterm.exe</code>, a PuTTY-like wrapper program
for Windows command prompts (or anything else running in a Windows
console). Not yet included in the installer, but available as a
<code>.exe</code> file from the Download page.
<li>Updated Unicode and bidi support to Unicode 14.0.0.
<li>New command-line option <code>-pwfile</code>, like
<code>-pw</code> except that it reads the password from a file so that
it doesn't show up on the command line.
<li>Windows Pageant: option <code>--openssh-config</code> to allow
easy interoperation with Windows's <code>ssh.exe</code>.
<li><code>-pw</code> (and <code>-pwfile</code>) now do not fall back
to interactively prompting for a password if the provided password
fails. (That was the original intention.)
<li>New configuration options for keyboard handling:
<ul>
<li>Option to control handling of Shift + arrow keys
<li>Extra mode in the function-keys option, for modern xterm (v216 and above).
</ul>
<li>Bug workaround flag to wait for the server's SSH greeting before
sending our own, for servers (or proxies) that lose outgoing data
before seeing any incoming data.
<li>Crypto update: added side-channel resistance in probabilistic RSA
key generation.
<li>Crypto update: retired the use of short Diffie-Hellman exponents
(just in case).
<li>Bug fix: reconfiguring remote port forwardings more than once no
longer crashes.
<li>Bug fix: terminal output processing is now paused while handling a
remote-controlled terminal resize, so that the subsequent screen
redraw is interpreted relative to the new terminal size instead of the old.
<li>Bug fix: Windows PuTTYgen's mouse-based entropy collection now
handles high-frequency mice without getting confused.
<li>Bug fix: Windows Pageant can now handle large numbers of
concurrent connections without hanging or crashing.
<li>Bug fix: if Windows Pageant is started multiple times
simultaneously, the instances should reliably agree on one of them to
be the persistent server.
<li>Bug fix: remote-controlled changes of window title are now
interpreted according to the configured character set.
<li>Bug fix: remote-controlled changes of window title no longer get
confused by UTF-8 characters whose encoding includes the byte 0x9C
(which terminates the control sequence in non-UTF-8 contexts).
<li>Bug fix: popping up the window context menu in the middle of a
drag-select now no longer leaves the drag in a stuck state.
<li>Bug fix: extensive use of true colour in the terminal no longer
slows down window redraws unnecessarily.
<li>Bug fix: when PSCP reports the server sending a disallowed
compound pathname, it correctly reports the replacement name it's
using for the downloaded file. 
<li>Bug fix: enabling X11 forwarding in <code>psusan</code> failed to
fall back through possible port numbers for the forwarded X display.
<li>For developers: migrated the build system to CMake, removing the
old idiosyncratic <code>mkfiles.pl</code> and the autotools system.
</ul>
</%method>

<%method newstitle>PuTTY 0.77 released</%method>
<%method newsbody>
<p>
PuTTY 0.77, released today, is a feature release. The headline feature
is improved proxy support: native support for forwarding a connection
through another SSH server, and the ability to prompt interactively
during proxy setup if the proxy requires a password or other login
information. Also, many bug fixes, some crypto improvements, and a new
application <code>pterm.exe</code> that wraps a Windows command
prompt.

</%method>
