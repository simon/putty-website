<%flags>
extends => '../relbase.mi'
</%flags>
<%class>method version() { return "0.50"; }</%class>
<%class>method date() { return "2000-10-16"; }</%class>
<%method changelog>
<ul>

<li>
Keep-alives to prevent overzealous idle detectors in firewalls from
closing connections down. Done by sending Telnet NOP or
SSH_MSG_IGNORE, so as to avoid affecting the actual data stream.

<li>
In PuTTY proper, in SSH mode, you can now specify a command to be
run at the remote end. (The SSH functionality was already there,
because it was required for PSCP and Plink. All it took was a bit of
GUI work to make it accessible from PuTTY itself.)

<li>
You can now configure the initial window title.

<li>
Running "<code>putty -cleanup</code>" will now remove all files and
registry entries created by PuTTY. If you've used PuTTY on somebody
else's machine and don't want to leave any mess behind, you can run
this before deleting the PuTTY executable.

<li>
The Event Log now scrolls down when new events appear on it, so that
if you leave it up all the time you can watch things happen. Also,
you can select items from the Event Log and copy them to the
clipboard (should help for debugging).

<li>
When using NT's opaque resize feature, resizing the window doesn't
send resize events at <em>every</em> step of the process, but
instead sends a single one at the end. (I'd have quite liked it to
do a resize event if the drag paused for maybe a second, but
WM_TIMER doesn't seem to get through in the middle of a resize. Oh
well, this is good enough.)

<li>
Everyone's favourite trivial change: Shift+Ins pastes. (No
configurable option to control this: it wasn't doing anything
interesting anyway.)

<li>
Included two extra Makefile options: <code>/DAUTO_WINSOCK</code>
makes the build process assume that <code>&lt;windows.h&gt;</code>
implicitly includes a WinSock header file, and
<code>/DWINSOCK_TWO</code> makes PuTTY include
<code>&lt;winsock2.h&gt;</code> instead of
<code>&lt;winsock.h&gt;</code>.

<li>
Bug fix for a bug <em>nobody</em> had ever noticed: if you hit About
twice, you only get one About box (as designed), <em>except</em>
that if you open and close the Licence box then PuTTY forgets about
the About box, so it will then let you open another. Now the
behaviour is sane, and you can never open more than one About box.

<li>
Bug fix: choosing local-terminal line discipline together with SSH
password authentication now doesn't cause the password to be echoed
to the screen.

<li>
Bug fix: network errors now do not close the window if Close On Exit
isn't set.

<li>
Bug fix: fonts such as 9-point (12-pixel) Courier New, which
previously failed to display underlines, now do so.

<li>
Bug fix: stopped the saved-configuration-name box getting blanked
when you swap away from and back to the Connection panel.

<li>
Bug fix: closing the About box returns focus to the config box, and
closing the View Licence box returns focus to the About box.

<li>
The moment you've <em>all</em> been waiting for: RSA public key
authentication is here! You can enter a public-key file name in the
SSH configuration panel, and PuTTY will attempt to authenticate with
that before falling back to passwords or TIS. Key file format is the
same as "regular" ssh. Decryption of the key using a passphrase is
supported. No key generation utility is provided, yet.

<li>
Created Pageant, a PuTTY authentication agent. PuTTY can use RSA
keys from this for authentication, and can also forward agent
communications to the remote end. Keys can be added and removed
either locally or remotely.

<li>
Created Plink, a command-line version of PuTTY suitable for use as a
component of a pipe assembly (for example, Windows NT CVS can use it
as a transport).

<li>
SSH protocol version 2 support. This is disabled by default unless
you connect to a v2-only server. Public key authentication isn't
supported (this places PuTTY technically in violation of the SSH-2
specification).

<li>
Enable handling of <code>telnet://hostname:port/</code> URLs on the
command line. With this feature, you can now set PuTTY as the
default handler for Telnet URLs. If you run the Registry Editor and
set the value in
<code>HKEY_CLASSES_ROOT\telnet\shell\open\command</code> to be
"<code>\path\to\putty.exe %1</code>" (with the full pathname of your
PuTTY executable), you should find that clicking on telnet links in
your web browser now runs PuTTY.

<li>
Re-merge the two separate forks of the ssh protocol code. PuTTY and
PSCP now use the same protocol module, meaning that further SSH
developments will be easily able to affect both.

</ul>

</%method>
