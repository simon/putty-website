<%flags>
extends => '../relbase.mi'
</%flags>
<%class>method version() { return "0.79"; }</%class>
<%class>method date() { return "2023-08-26"; }</%class>
<%method changelog>
<ul>
<li>Windows installer scope is back to the normal 'per machine'
setting, reverting 0.78's security workaround.
<p><b>Note:</b> this means that installing the 0.79 or later Windows
installer will <em>not</em> automatically uninstall 0.78, if 0.78 was
installed using its default 'per user' scope. In that situation we
recommend uninstalling 0.78 first, if possible. If both end up
installed, uninstalling both and then re-installing the new version
will fix things up.</p></li>
<li>Terminal mouse tracking: support for mouse movements which are not
drags.</li>
<li>Terminal mouse tracking: support for horizontal scroll events
(e.g. generated by trackpads).</li>
<li>Backwards compatibility fix: certificate-based user authentication
now works with OpenSSH 7.7 and earlier.</li>
<li>Bug fix: in a session using the 'Raw' protocol, pressing ^D twice
in the terminal window could cause an assertion failure.</li>
<li>Bug fix: terminal output could hang if a resize control sequence
was sent by the server (and was not disabled in the Features panel)
but PuTTY's window was set to non-resizable in the Window panel.</li>
<li>Bug fix: GTK PuTTY could fail an assertion if a resize control
sequence was sent by the server while the window was docked to one
half of the screen in KDE.</li>
<li>Bug fix: GTK PuTTY could fail an assertion if you tried to change
the font size while the window was maximised.</li>
<li>Bug fix: the 'bell overload' timing settings were misinterpreted
by Unix PuTTY and pterm 0.77/0.78; if any settings were saved using
these versions, confusion can persist with newer versions.</li>
<li>Bug fix: SSH authentication banners were not reliably printed if a
server sent one immediately before closing the connection (e.g.
intended as a user-visible explanation for the connection
closure).</li>
<li>Bug fix: the 'close' command in PSFTP always reported failure, so
that ending a <code>psftp -b</code> batch script with it would cause
PSFTP as a whole to believe it had failed, even if everything worked
fine.</li>
<li>Bug fix: certificate handling would do the wrong thing, for RSA
keys only, if you specified a detached certificate to go with a PPK
file that had a different certificate embedded.</li>
<li>Bug fix: Windows Pageant's option to write out a configuration
file fragment for Windows OpenSSH now works even if you have a space
in your user name.</li>
<li>Bug fix: in local-line-editing mode, pressing ^U now just clears
the line, instead of clearing it and then inserting a literal ^U.</li>
<li>Several bug fixes in edge cases of terminal wrapping, involving
double-width characters.</li>
</ul>
</%method>

<%method newstitle>PuTTY 0.79 released</%method>
<%method newsbody>
<p>
PuTTY 0.79, released today, is mostly a bug fix release, with only minor new features in SSH and terminal mouse handling.
<p>
The most important bug fix is that we've restored the Windows 'install
scope' to the way it was in 0.77 and earlier, reverting the security
workaround we had to put into 0.78. This means the 0.79 Windows
installer will <em>not</em> uninstall 0.78 automatically, so we
recommend uninstalling 0.78 by hand first, if you have it installed.
As before, if you've ended up with both versions installed,
uninstalling them both and then running the new installer will put
everything right.
</%method>
