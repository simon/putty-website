<%flags>
extends => '../relbase.mi'
</%flags>
<%class>method version() { return "0.67"; }</%class>
<%class>method date() { return "2016-03-05"; }</%class>
<%method changelog>
<ul>
<li>Security fix: a buffer overrun in the old-style SCP protocol when
receiving the header of each file downloaded from the server is fixed.
See <a href="<% $::MAIN_URI %>wishlist/vuln-pscp-sink-sscanf.html">vuln-pscp-sink-sscanf</a>.
<li>Windows PuTTY now sets its process ACL more restrictively, in an
attempt to defend against malicious other processes reading sensitive
data out of its memory.
<li>Assorted other robustness fixes for crashes and memory leaks.
<li>We have started using Authenticode to sign our Windows executables
and installer.
</ul>
</%method>

<%method newstitle>PuTTY 0.67 released, fixing a SECURITY HOLE</%method>
<%method newsbody>
<p>
PuTTY 0.67, released today, fixes a <b>security hole</b> in 0.66 and
before:
<a href="wishlist/vuln-pscp-sink-sscanf.html">vuln-pscp-sink-sscanf</a>. It
also contains a few other small bug fixes.
<p>
Also, for the first time, the Windows executables in this release
(including the installer) are signed using an Authenticode
certificate, to help protect against tampering in transit from our
website or after downloading. You should find that they list "Simon
Tatham" as the verified publisher.
</%method>
