<%flags>
extends => '../relbase.mi'
</%flags>
<%class>method version() { return "0.74"; }</%class>
<%class>method date() { return "2020-06-27"; }</%class>
<%method changelog>
<ul>
<li>Security fix: if an SSH server accepted an offer of a public key
and then rejected the signature, PuTTY could access freed memory, if
the key had come from an SSH agent.</li>
<li>Security feature: new config option to disable PuTTY's dynamic
host key preference policy, if you prefer to avoid giving away to
eavesdroppers which hosts you have stored keys for.</li>
<li>Bug fix: the installer UI was illegible in Windows high-contrast
mode.</li>
<li>Bug fix: console password input failed on Windows 7.</li>
<li>Bug fixes in the terminal: one instance of the dreaded
"line==NULL" error box, and two other assertion failures.</li>
<li>Bug fix: potential memory-consuming loop in bug-compatible padding
of an RSA signature from an agent.</li>
<li>Bug fix: PSFTP's buffer handling worked badly with some servers
(particularly proftpd's <code>mod_sftp</code>).</li>
<li>Bug fix: cursor could be wrongly positioned when restoring from
the alternate terminal screen. (A bug of this type was fixed in 0.59;
this is a case that that fix missed.)</li>
<li>Bug fix: character cell height could be a pixel too small when
running GTK PuTTY on Ubuntu 20.04 (or any other system with a
similarly up-to-date version of Pango).</li>
<li>Bug fix: old-style (low resolution) scroll wheel events did not
work in GTK 3 PuTTY. This could stop the scroll wheel working at all
in VNC.</li>
</ul>
</%method>

<%method newstitle>PuTTY 0.74 released</%method>
<%method newsbody>
<p>
PuTTY 0.74, released today, is a bug-fix and security release. It
fixes bugs in 0.73, including one possible vulnerability, and also
adds a
<a href="<% $::BIN_URI %>0.74/htmldoc/Chapter4.html#config-ssh-prefer-known-hostkeys">new configuration option</a>
to mitigate a minor information leak in SSH host key policy.
</%method>
