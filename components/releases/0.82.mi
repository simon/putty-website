<%flags>
extends => '../relbase.mi'
</%flags>
<%class>method version() { return "0.82"; }</%class>
<%class>method date() { return "2024-11-27"; }</%class>
<%method changelog>
<ul>
<li>Major refactoring of Unicode handling to allow the use of 'foreign' Unicode characters, i.e. outside the system's configured default character set / code page. Not yet complete, but the following things work:
<ul>
<li>Windows console: usernames and passwords entered interactively into PSCP, PSFTP and Plink can use foreign characters.
<li>Windows command line: usernames, remote commands, and filenames entered via command-line options can use foreign characters.
<li>PuTTY's own terminal (on Windows and Unix): even if it's not configured into UTF-8 mode for the main session, interactive usernames and passwords can use foreign characters.
</ul>
<li>Unicode version update: all character analysis is updated to Unicode 16.0.0.
<li>Unicode terminal rendering: national and regional flags are now understood by PuTTY's terminal emulator. (However, correct display of those flags will depend on fonts and operating system.)
<li>The Event Log mentions the local address and port number of the outgoing connection socket.
<li>Bracketed paste mode can now be turned off in the Terminal &gt; Features panel.
<li>Unix Pageant: new <code>--foreground</code> mode for running as a subprocess.
<li>Bug fix: the 'border width' configuration option is now honoured even when the window is maximised.
<li>Bug fix: SHA-2 based RSA signatures are now sent with correct zero padding.
<li>Bug fix: terminal wrap mishandling caused occasional incorrect redraws in curses-based applications.
<li>Bug fix: Alt + function key in "Xterm 216+" mode sent a spurious extra escape character.
</ul>
</%method>

<%method newstitle>PuTTY 0.82 released</%method>
<%method newsbody>
<p>
PuTTY 0.82, released today, improves Unicode support. Usernames and
passwords read from the terminal or the Windows console now support
full Unicode, so that you can use characters outside the Windows
system code page, or the character set configured in PuTTY. The same
is true for usernames and file names provided via the PuTTY tools'
command line and via the GUI. (However, unfortunately not yet if you
save and reload a session.)
</%method>
