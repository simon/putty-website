<%class>
has 'filename' => (required => 1);
method name() { return undef; } # not equal to any nav bar link
method outpath() { return "wishlist/" . $.filename; }
has 'filedata' => (default => sub { {} });
method readfile() {
  return if exists $.filedata->{'title'};
  my $srcfile = "$::WISHLIST/" . $.filename;
  open my $fh, $srcfile or die "$srcfile: open: $!\n";
  my $title;
  while (<$fh>) {
    $title = $1 if m!<title>(.*)</title>!;
    last if m!<a name="endheader">!i;
  }
  my $text = "";
  my $audit;
  my $timestamp;
  while (<$fh>) {
    $audit = $_, next if m!^<div class=\"audit\">(.*)</div>$!;
    $timestamp = $_, next if m!^<div class=\"timestamp\">(.*)</div>$!;
    last if m!^</body>!;
    $text .= $_;
  }
  close $fh;
  $.filedata->{"text"} = $text;
  $.filedata->{"audit"} = $audit;
  $.filedata->{"timestamp"} = $timestamp;
  $.filedata->{"title"} = $title;
}
method w_title() { $.readfile(); $.filedata->{'title'}; }
method w_audit() { $.readfile(); $.filedata->{'audit'}; }
method w_timestamp() { $.readfile(); $.filedata->{'timestamp'}; }
method w_text() { $.readfile(); $.filedata->{'text'}; }
</%class>
<%method title><% $.w_title() %></%method>
<%method extrafooter><% $.w_audit() %></%method>
<%method timestamp><% $.w_timestamp() %></%method>

<% $.w_text() %>
