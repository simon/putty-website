<%class>method name() { return "Team"; }</%class>
<%method title>PuTTY Team Members</%method>

<p>
PuTTY is maintained by a small team based in Cambridge, England.  Between us,
we handle writing PuTTY, updating its Web site, recording its bugs,
and reading (and often responding to) messages to the PuTTY contact e-mail
address.  We are:

<dl>
<dt style="font-style: italic">Simon Tatham</dt>
<dd>Project originator, main developer and benevolent dictator.
Wrote the majority of the code; does the majority of new development
and features. Also maintains
<a href="https://www.chiark.greenend.org.uk/~sgtatham/halibut/">Halibut</a>,
the documentation system used to produce the Windows Help file and
the HTML manual.
</dd>

<dt style="font-style: italic; padding-top: 1em">Alexandra Lanes</dt>
<dd>Looks after the nightly snapshot builds, answers mail.  Provider of 
common sense.  Programmer, cardboard and sometimes real.
</dd>

<dt style="font-style: italic; padding-top: 1em">Ben Harris</dt>
<dd>Ben did a lot of the early work on the (still unfinished) Macintosh
port and is reputed
to understand ISO 2022.  He's also the maintainer of an enormous
<a href="https://bjh21.me.uk/all-escapes/all-escapes.txt">concordance</a>
of escape sequences
and of <a href="http://www.netbsd.org/Ports/acorn26/">NetBSD/acorn26</a>.
</dd>

<dt style="font-style: italic; padding-top: 1em">Jacob Nevins</dt>
<dd>Answers mail, herds wishlist, occasionally writes code.
</dd>
</dl>
