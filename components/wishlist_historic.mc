<%class>method name() { return "WishlistHistoric"; }</%class>
<%method title>PuTTY Historic Wish List</%method>

% if (defined $::WISHLIST) {
<p>
This is the "historic" section of the PuTTY wishlist page: things
fixed long ago, and things that aren't relevant any more. Check the
<a href="<% $.relpath($.outpath(), $.navpath("Wishlist")) %>">main Wishlist page</a>
for more recent releases and still-relevant open issues.

<%perl>

my $srcfile = "$::WISHLIST/historic.html";
open my $fh, $srcfile or die "$srcfile: open: $!\n";
while (<$fh>) {
  last if m!<a name="endheader">!i;
}

while (<$fh>) {
  last if m!^</body>!;
  # for each line, we must rewrite hrefs
  s!<a href="\Q${main::MAIN_URI}\E([^"]*)">!'<a href="' . $.relpath($.outpath(), $1) . '">'!eg;
  print;
}

close $fh;

</%perl>
% } else {

<div class="missing">
To generate the parts of this website derived from the separate PuTTY
wishlist repository, define <code>$WISHLIST</code> in
<code>config.pl</code>.
</div>

% }
