<%class>method name() { return "Updates"; }</%class>
<%method title>PuTTY Updates</%method>

<p>
If you would like to be told when new releases of PuTTY come out,
you can subscribe to the PuTTY-announce mailing list. Do this by
going to the
<a href="<% $::MAILLIST_URI %>">list information and subscription page</a>.
Please do <em>not</em> request a subscription by mailing the PuTTY
authors.

<p>
Please do <em>not</em> send mail to PuTTY-announce! It is not a
discussion list. It is not a place to report bugs to. It is a list
of people who want to receive one e-mail, from the PuTTY developers,
every time there is a new release. All posts from other people, even
list members, will be rejected.  If you want to contact the PuTTY team,
see the <a href="feedback.html">Feedback page</a>.

<p>
Please do not subscribe an address to the PuTTY-announce list if it
has a spam filter which requires the sender to confirm incoming
messages. The PuTTY team does not have the time to reply to all such
confirmation requests. If we receive confirmation messages in
response to a release announcement, <em>we will ignore them</em>,
and you won't receive the mail. It is your responsibility to ensure
that an address subscribed to PuTTY-announce can receive mail from
it without our personal attention.

<p>
If you want to modify or cancel your subscription, you can do all of
this from the
<a href="<% $::MAILLIST_URI %>">mailing list information page</a>.
