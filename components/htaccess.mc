<%override wrap>%
% $m->result->data->{'outpath'} = '.htaccess';
% unless ($::is_mirror) {
%   my $mainpath = $::MAIN_URI->path;
%   foreach my $binary (qw(putty puttytel psftp plink pscp pageant puttygen)) {
Redirect permanent <% $mainpath %>latest/<% $binary %>.exe <% $::MAIN_URI %>latest/x86/<% $binary %>.exe
Redirect permanent <% $mainpath %>latest/<% $binary %>-alpha.exe <% $::MAIN_URI %>latest/alpha/<% $binary %>.exe
%   }
Redirect permanent <% $mainpath %>latest/putty.zip <% $::MAIN_URI %>latest/putty-src.zip
%   unless ($::MAIN_URI->eq($::BIN_URI)) {
Redirect permanent <% $mainpath %>latest/ <% $::BIN_URI %>latest/
%     foreach my $release ($.list_releases()) {
Redirect temp <% $mainpath %><% $release->version() %>/ <% $::BIN_URI %><% $release->version() %>/
%     }
%   }
Redirect permanent <% $mainpath %>bugs.html <% $::MAIN_URI %>feedback.html
Redirect permanent <% $mainpath %>wishlist.html <% $::MAIN_URI %>wishlist/
Redirect permanent <% $mainpath %>download.html <% $::MAIN_URI %>latest.html
% }
AddType application/octet-stream .chm
AddType application/octet-stream .hlp
AddType application/octet-stream .cnt
AddType application/pgp-signature .gpg
</%override>
