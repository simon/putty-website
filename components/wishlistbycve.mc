<%class>
method name() { return undef; } # not equal to any nav bar link
method outpath() { return "wishlist/cvelist.html"; }
</%class>
<%method title>PuTTY wishlist entries indexed by CVE</%method>

% if (defined $::WISHLIST) {
<p>
This page lists all the CVE vulnerability identifiers that are
mentioned by PuTTY wishlist entries, and links each one to the
wishlist entry (or entries) that mention it.

<p>
If you want to find our own writeup for a vulnerability, and you
already know its CVE number, then this page may be a convenient way to
find it.

<p>
<%perl>

my $srcfile = "$::WISHLIST/cvelist.html";
open my $fh, $srcfile or die "$srcfile: open: $!\n";
while (<$fh>) {
  last if m!<a name="endheader">!i;
}

while (<$fh>) {
  last if m!^<a name="startfooter">!;
  # for each line, we must rewrite hrefs
  s!<a href="\Q${main::MAIN_URI}\E([^"]*)">!'<a href="' . $.relpath($.outpath(), $1) . '">'!eg;
  print;
}

close $fh;

</%perl>
% } else {

<div class="missing">
To generate the parts of this website derived from the separate PuTTY
wishlist repository, define <code>$WISHLIST</code> in
<code>config.pl</code>.
</div>

% }
