<%flags>
extends => 'release.mc'
</%flags>
<%class>
has 'release' => (required => 0, default => undef);
method name() { return "Stable"; }
method httppath() {
    # In a mirror site, the links to the latest version are relative,
    # so that the binary and docs downloads come from the mirror site
    # itself.
    #
    # Also, we should link directly to the release by number, rather
    # than relying on a redirect, so that mirror sites don't need a
    # working .htaccess.
    if ($::is_mirror) {
        return $.relpath($.outpath(), $.latest_version() . "/");
    }
    # Otherwise, link to the primary site and use the "latest" redirect.
    return ${main::BIN_URI} . "latest/";
}
</%class>
<%method title>Download PuTTY: latest release (<% $.latest_version() %>)</%method>
<%method header>
<p>
% my $rel = $m->construct("/releases/" . $.release . ".mi");
This page contains download links for the latest released version of
PuTTY.
Currently this is <% $.latest_version() %>, released on <% $rel->date() %>.

<p>
When new releases come out, this page will update to contain the
latest, so this is a good page to bookmark or link to.
Alternatively, here is a
<a href="<% $.relpath($.outpath(), $.navpath("Release " . $.latest_version())) %>">permanent link to the <% $.latest_version() %> release</a>.
</%method>

<% $self->SUPER::main() %>

<%init>
$.release($.latest_version());
</%init>
