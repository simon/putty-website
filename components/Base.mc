<%method navlink ($name)>
% if (defined $.name() && $.name() eq $name) {
  <b><% $name %></b>
% } else {
<a href="<% $.relpath($.outpath(), $.navpath($name)) %>"><% $name %></a>
% }
</%method>

<%method timestamp>(last modified on <!--LASTMOD-->[insert date here]<!--END-->)</%method>
<%method extrafooter></%method>

<%augment wrap><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML><HEAD>
<TITLE><% $.title %></TITLE>
<link rel="canonical" href="<% $.canonicallink() %>">
<link rel="stylesheet" type="text/css" href="<% $.relpath($.outpath(), "sitestyle.css") %>" title="PuTTY Home Page Style">
<link rel="shortcut icon" href="putty.ico">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
% if ($::is_mirror) {
<meta name="robots" content="noindex,nofollow">
% }
</HEAD>
<BODY>
<h1 align=center><% $.title %></h1>

% if ($::is_mirror) {
<div align=center class="mirrorwarning">
This is a mirror. Follow this link to find
<a href="<% $::MAIN_URI %>">the primary PuTTY web site</a>.
</div>
% }

<p align=center>
<% $.navlink("Home") %> |
<% $.navlink("FAQ") %> |
<% $.navlink("Feedback") %> |
<% $.navlink("Licence") %> |
<% $.navlink("Updates") %> |
<% $.navlink("Mirrors") %> |
<% $.navlink("Keys") %> |
<% $.navlink("Links") %> |
<% $.navlink("Team") %><br>
Download:
<% $.navlink("Stable") %> &#183;
% if (defined $.prerel_version()) {
<% $.navlink("Pre-release") %> &#183;
% }
<% $.navlink("Snapshot") %> |
<% $.navlink("Docs") %> |
<% $.navlink("Privacy") %> |
<% $.navlink("Changes") %> |
<% $.navlink("Wishlist") %></p>

<% inner() %>

<p><hr>
If you want to comment on this web site, see the
% if (defined $.name() && $.name() eq "Feedback") {
instructions above.
% } else {
<a href="<% $.relpath($.outpath(), $.navpath("Feedback")) %>">Feedback page</a>.
% }
<br>
<% $.extrafooter %>
<% $.timestamp %>
</BODY></HTML>
% $m->result->data->{'outpath'} = $.outpath();
</%augment>
<%class>
method list_releases() {
  return sort { $b->date() cmp $a->date() }
          map { $m->construct($_) }
              $m->interp()->glob_paths("/releases/*.mi");
}
method latest_version() {
  my @releases = $.list_releases();
  return $releases[0]->version();
}
method prerel_version() {
  # Define this to a version string such as "0.67" to enable the parts
  # of the website that announce pre-release builds of that version.
  # Alternatively, set to undef to disable those parts.
  return undef;
}
method relpath() {
  my ($from, $to) = @_;
  my $prefix = "http://host/toplevel/";
  my $rel = URI->new($prefix . $to)->rel($prefix . $from);
  $rel =~ s!/./!/!g;
  return $rel;
}
method outpath() {
  # Pages without a navpath need to define their own version of outpath()
  my $ret = $.navpath($.name());
  if ($ret =~ m!/$!) { $ret .= "index.html"; }
  $ret =~ s!^./!!;
  return $ret;
}
method navpath($name) {
  my $fixed = {
    "Home" => "./$::indexhtml",
    "Licence" => "licence.html",
    "FAQ" => "faq.html",
    "Docs" => "docs.html",
    "Keys" => "keys.html",
    "Links" => "links.html",
    "Mirrors" => "mirrors.html",
    "Updates" => "maillist.html",
    "Feedback" => "feedback.html",
    "Privacy" => "privacy.html",
    "Changes" => "changes.html",
    "Wishlist" => "wishlist/$::indexhtml",
    "WishlistHistoric" => "wishlist/historic.html",
    "Team" => "team.html",
    "Stable" => "latest.html",
    "Snapshot" => "snapshot.html",
    "Pre-release" => "prerel.html",
    "Privacy" => "privacy.html",
  }->{$name};
  return $fixed if defined $fixed;
  return "releases/$1.html" if ($name =~ m!Release (.*)$!);
  die "no navpath for '$name'";
}
method canonicallink() {
  my $uri = URI->new_abs($.outpath(), $::MAIN_URI);
  $uri =~ s!/index\.html$!/$::indexhtml!;
  return $uri;
}
method colourcategory($version) {
  if (defined $version and
      defined $::vulnlists{$version} and
      $::vulnlists{$version} ne "") {
    return "insecure";
  } elsif ($version eq "snapshots") {
    return "snapshot";
  } elsif (defined $.prerel_version() and
          $version eq $.prerel_version()) {
    return "prerel";
  } elsif ($version eq $.latest_version()) {
    return "latest";
  } else {
    return "older";
  }
}
</%class>
<%init>
</%init>
