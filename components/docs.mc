<%class>method name() { return "Docs"; }</%class>
<%method title>PuTTY Documentation Page</%method>

<p>
This page links to the documentation for all current versions of
PuTTY. The same links are available on the download pages.

% my $latest_rel_mc = $m->construct("/latest.mc");
% my $latest_rel_vmi = $m->construct("/version.mi", page=>$latest_rel_mc, category=>$.colourcategory($.latest_version()), release=>$latest_rel_mc->release, callername=>$.name());
<% $latest_rel_vmi->docsbox("Documentation for the latest release (" . $.latest_version() . ")") %>

% if (defined $.prerel_version()) {
% my $prerel_mc = $m->construct("/prerel.mc");
% my $prerel_vmi = $m->construct("/version.mi", page=>$prerel_mc, category=>$.colourcategory($.prerel_version()), callername=>$.name());
<% $prerel_vmi->docsbox("Documentation for pre-releases of " . $.prerel_version()) %>
% }

% my $snapshot_mc = $m->construct("/snapshot.mc");
% my $snapshot_vmi = $m->construct("/version.mi", page=>$snapshot_mc, category=>$.colourcategory("snapshots"), callername=>$.name());
<% $snapshot_vmi->docsbox("Documentation for the development snapshots") %>

<p>
Note that some versions of Windows will refuse to run HTML Help
files (<code>.CHM</code>)
if they are installed on a network drive. If you have installed
PuTTY on a network drive, you might want to check that the help file
works properly. If not, see
<a href="http://support.microsoft.com/kb/896054">MS KB 896054</a>
for information on how to solve this problem.
